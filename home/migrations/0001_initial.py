# Generated by Django 2.2.5 on 2019-09-25 19:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_time', models.DateField()),
                ('id_sinapsis', models.CharField(max_length=100)),
                ('level1', models.FloatField()),
                ('level2', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_sinapsis', models.CharField(max_length=20)),
                ('date_time', models.DateField()),
                ('temp_ext', models.FloatField()),
                ('temp_int', models.FloatField()),
                ('status', models.IntegerField()),
            ],
        ),
    ]
