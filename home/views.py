import psycopg2, json

from django.shortcuts import render
from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder


from .models import Status, Device


def home_view(request):

	supra_installed = ["VKA-B-A011-CA53-91A8", "VKA-B-A011-CA01-DD12", "38", "26", "14", "VKA-B-A011-CA4B-7FEA"]
	hatillo_installed = ["VKA-B-A011-CA01-CBBA", "VKA-B-0000-0000-2269", "VKA-B-P219-BA04-08F6", "VKA-B-A011-CA04-065E", "VKA-B-A011-CA04-08DE", "VKA-B-A011-CA01-DCFA"]
	prueba_installed = ["VKA-B-A011-CA53-915C", "VKA-B-A011-CA04-08F2"]

	context = {'supra_installed':supra_installed,
				'hatillo_installed':hatillo_installed,
				'prueba_installed':prueba_installed
				}

	return render(request,'home.html', context)




def ajax_jtep_report(request):
	id_indb = Device.objects.order_by('id_sinapsis','-date_time')\
		.distinct('id_sinapsis')\
		.values('date_time','company','id_sinapsis', 'espec', 'mode')


	return HttpResponse(json.dumps(list(id_indb),cls=DjangoJSONEncoder))
