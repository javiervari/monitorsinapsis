from django.urls import path

from home.views import home_view, ajax_jtep_report

app_name = 'monitor'

urlpatterns = [
    path('', home_view, name='home_view'),
    path('ajax_jtep_report', ajax_jtep_report, name='ajax_jtep_report'),

]