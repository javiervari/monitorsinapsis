from django.db import models


class Device(models.Model):
    id_sinapsis = models.CharField(max_length=20)
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    coord = models.CharField(max_length=100)
    company = models.CharField(max_length=20)
    espec = models.CharField(max_length=20)
    mode = models.CharField(max_length=20)

    


class Status(models.Model):
    id_sinapsis = models.CharField(max_length=20)
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    temp_ext = models.FloatField()
    temp_int = models.FloatField()
    status = models.IntegerField()

