import json, time
from datetime import datetime
import psycopg2
import paho.mqtt.client as mqtt
from cryptography.fernet import Fernet


cipher_suite = Fernet("UOC-DMXptS_t7JPi_omDofSfTYaYVTqn638NLMl_zYE=")

BROKER = 'sinapsis.vikua.com'
TOPIC = 'vikua/jtep/sinapsis/b/status'
PORT = '1883'

conn = psycopg2.connect(host='localhost',
                        dbname='monitor',
                        user='pi',
                        password='Password123'
                        )
conn.autocommit = True
cursor = conn.cursor()
connected = 0



def on_connect(client, userdata, flags, rc):
    global connected
    if rc==0:
        connected = 1
        print("MQTT Connected, code =",rc)
        print("subscribing",TOPIC)
        client.subscribe(TOPIC, 1)
    else:
        print("Bad connection Returned code=",rc)
        connected = 0

def on_message(client, userdata, message):
    msg = cipher_suite.decrypt(message.payload)
    msg = json.loads(msg.decode('utf-8'))
    try:
        id_sinapsis = msg['id']
        try:
            temp_ext = msg['variables']['temperature_ext']
        except:
            temp_ext=0
        temp_int = msg['variables']['temperature']
        status = msg['status']

        q = '''INSERT INTO PUBLIC.HOME_STATUS (ID_SINAPSIS,
                                                DATE_TIME,
                                                TEMP_EXT,
                                                TEMP_INT,
                                                STATUS)
                VALUES(
                    '{}',
                    NOW() AT TIME ZONE 'AMERICA/CARACAS',
                    {},{},{}
                    )'''.format(id_sinapsis,
                                temp_ext,
                                temp_int,
                                status
                                )
        cursor.execute(q)
    except Exception as e:
        print(e,"<<<<<<<<<<<")




client = mqtt.Client()
client.on_message = on_message
client.on_connect = on_connect


while True:
    while connected==1:
        time.sleep(30)
        
    while connected==0:
        client.connect(BROKER)
        client.loop_start()
        time.sleep(2)


