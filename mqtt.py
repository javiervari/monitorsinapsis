import json, time, pytz
from datetime import datetime
import psycopg2
import paho.mqtt.client as mqtt
from cryptography.fernet import Fernet


cipher_suite = Fernet("UOC-DMXptS_t7JPi_omDofSfTYaYVTqn638NLMl_zYE=")

BROKER = 'sinapsis.vikua.com'
TOPIC = '#'
PORT = '1883'

conn = psycopg2.connect(host='localhost',
                        dbname='monitor',
                        user='pi',
                        password='Password123'
                        )
conn.autocommit = True
cursor = conn.cursor()


connected = 0

sinapsis_instalados_supra=["VKA-B-A011-CA53-91A8", "VKA-B-A011-CA01-DD12", "38", "26", "14", "VKA-B-A011-CA4B-7FEA"]
sinapsis_instalados_hatillo=["VKA-B-A011-CA01-CBBA", "VKA-B-0000-0000-2269", "VKA-B-P219-BA04-08F6","VKA-B-A011-CA04-065E", "VKA-B-A011-CA04-08DE", "VKA-B-A011-CA01-DCFA"]
sinapsis_prueba = ["VKA-B-A011-CA53-915C", "VKA-B-A011-CA04-08F2"]

sinapsis_no_utc = ["38", "26", "14", "VKA-B-0000-0000-2269", "VKA-B-P219-BA04-08F6"]


def store_device(msg, company):
    id_sinapsis = msg['sinapsis']['id']
    date = msg['message_details']['date']
    coord = "'{};{}'".format(msg['devices']['gps']['latitude'],msg['devices']['gps']['longitude'])
    
    try:
        mode = msg['sinapsis']['mode']
    except:
        mode = 'normal'



    if id_sinapsis in sinapsis_no_utc:
        datetime_obj = datetime.strptime(date, '%Y-%m-%d %H:%M:%S.%f')
        date = datetime_obj.astimezone(pytz.UTC) #UTC





    #ESPEC
   
    if id_sinapsis == 'VKA-B-A011-CA01-DD12':
        espec = '443'
    elif id_sinapsis == 'VKA-B-A011-CA53-91A8':
        espec = '441'
    elif id_sinapsis == '38':
        espec = '447'

    elif id_sinapsis == '26':
        espec = 'Misterio?'

    elif id_sinapsis == 'VKA-B-P219-BA04-08F6':
        espec = '630'
    elif id_sinapsis == 'VKA-B-A011-CA04-065E':
        espec = '0096'
    elif id_sinapsis == 'VKA-B-A011-CA01-CBBA':
        espec = '108'
    elif id_sinapsis == 'VKA-B-0000-0000-2269':
        espec = '107'

    elif id_sinapsis == 'VKA-B-A011-CA4B-7FEA':
        espec = 'SUPRA'
    elif id_sinapsis == 'VKA-B-A011-CA04-08DE':
        espec = 'Bus'
    elif id_sinapsis == 'VKA-B-A011-CA01-DCFA':
        espec = 'Patrulla'

    elif id_sinapsis == 'VKA-B-A011-CA53-915C':
        espec = 'Inaki'

    else:
        espec = 'N/A'


    
    q = """INSERT INTO PUBLIC.HOME_DEVICE (ID_SINAPSIS, DATE_TIME, COORD, COMPANY, ESPEC, MODE)
                VALUES(
                    '{}',
                    '{}',
                    {},
                    '{}',
                    '{}',
                    '{}'
                    )""".format(id_sinapsis, date, coord, company,espec, mode)
    cursor.execute(q)
    return



def on_connect(client, userdata, flags, rc):
    global connected
    if rc==0:
        connected = 1
        print("MQTT Connected, code =",rc)
        print("subscribing",TOPIC)
        client.subscribe(TOPIC, 1)
    else:
        print("Bad connection Returned code=",rc)
        connected = 0






def on_disconnect(self, client, userdata, rc):
    connected = 0





def on_message(client, userdata, message):
    try:
        msg = cipher_suite.decrypt(message.payload)
        msg = json.loads(msg.decode('utf-8'))

        if message.topic=='vikua/jtep/sinapsis/b/device':
            


            if msg['sinapsis']['id'] in sinapsis_instalados_supra:
                store_device(msg, 'SUPRA')

            elif msg['sinapsis']['id'] in sinapsis_instalados_hatillo:
                store_device(msg, 'HATILLO')

            elif msg['sinapsis']['id'] in sinapsis_prueba:
                store_device(msg, 'PRUEBA')


    except Exception as e:
        print(e,"------->", msg['sinapsis']['id'])
    




client = mqtt.Client()
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_message = on_message


while True:
    while connected==1:
        time.sleep(30)
        
    while connected==0:
        client.connect(BROKER)
        client.loop_start()
        time.sleep(2)


